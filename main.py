import base64
import json
import os
import random

import pyperclip
import rsa
import webbrowser

KEY_PATH = './key.pub'
DATA_PATH = './data.txt'


def print_help():
    print('Hi from CryptoNotebook!')
    print('Possible commands:')
    print(' - ADD <link> <description>        - saves link to notebook with description')
    print(' - PAST <description>              - takes link from clipboard, clears clipboard')
    print(' - LIST                            - prints all the links and description')
    print(' - OPEN                            - opens all the links in browser')
    print(' - HELP                            - prints this help')
    print(' - EXIT                            - quits app')


def open_browser(links):
    chrome_path = 'C:/Program Files (x86)/Google/Chrome/Application/chrome.exe %s'
    webbrowser.get(chrome_path + ' --incognito').open('https://yandex.ru')
    for link in links:
        webbrowser.get(chrome_path + ' --incognito').open_new_tab(link.split()[0])


def random_triplet(passphrase):
    rand = random.Random(int.from_bytes(passphrase.encode(), byteorder='big'))
    return rand.randint(0, 10**50), rand.randint(0, 10**50), rand.randint(0, 10**50)


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    if not os.path.isfile(KEY_PATH):
        print('Hi! Looks like you did not generate keys yet. Let\'s do it!')
        passphrase = input('Enter passphrase >> ')

        (_, privkey) = rsa.newkeys(4096)
        rd, rp, rq = random_triplet(passphrase)

        with open(KEY_PATH, 'w') as f:
            json.dump({
                'p': privkey.p ^ rp,
                'd': privkey.d ^ rd,
                'e': privkey.e,
                'n': privkey.n,
                'q': privkey.q ^ rq
            }, f)

    with open(KEY_PATH, 'r') as f:
        key_file = json.load(f)
    pubkey = rsa.PublicKey(key_file['n'], key_file['e'])
    print_help()
    s = input('Enter command >> ').lower()
    while s != 'exit':
        cmd, arg1, arg2 = s.split()[0], ' '.join(s.split()[1:2]), ' '.join(s.split()[2:])
        if cmd == 'past':
            arg2 = arg1 + ' ' + arg2
            arg1 = pyperclip.paste()
            pyperclip.copy("")
        if cmd == 'past' or cmd == 'add':
            msg = rsa.encrypt((arg1 + '  -  ' + arg2).encode('utf-8'), pubkey)
            with open(DATA_PATH, 'a') as f:
                f.write(base64.b64encode(msg).decode('utf-8') + '\n')
        if cmd == 'list' or cmd == 'open':
            passphrase = input('Enter passphrase >> ')
            rd, rp, rq = random_triplet(passphrase)
            privkey = rsa.PrivateKey(pubkey.n, pubkey.e, key_file['d'] ^ rd, key_file['p'] ^ rp,
                                     key_file['q'] ^ rq)
            with open(DATA_PATH, 'r') as f:
                lines = f.readlines()
            lines = [rsa.decrypt(base64.b64decode(msg[:-1].encode('utf-8')), privkey).decode('utf-8') for msg in lines]
            if cmd == 'list':
                print('\n'.join(lines))
            if cmd == 'open':
                open_browser(lines)
        if cmd == 'help':
            print_help()

        s = input('Enter next command >> ').lower()


